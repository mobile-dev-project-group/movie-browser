package com.example.moviebrowser;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.BundleCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.moviebrowser.model.Movie;

import java.util.List;

public class BrowseFragment extends Fragment {
    public BrowseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_browse, container, false);

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).loadFragmentWithBackStack(new NewItemFragment(), "newItem");
            }
        });

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Refresh();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void Refresh()
    {
        TextView emptyList = getActivity().findViewById(R.id.text_empty_list);
        final ListView movieList = getActivity().findViewById(R.id.movies_list);
        final List<Movie> movies = ((MainActivity)getActivity()).sqliteDB.getAllMovie();

        if (movies == null || movies.size() == 0) {
            emptyList.setVisibility(View.VISIBLE);
            movieList.setVisibility(View.INVISIBLE);
        }
        else {
            emptyList.setVisibility(View.INVISIBLE);
            movieList.setVisibility(View.VISIBLE);
            MovieAdapter arrayAdapter = new MovieAdapter(getContext(), movies);
            movieList.setAdapter(arrayAdapter);

            movieList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Movie movie = movies.get(position);
                    Fragment fragment = new MovieDetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("movieId", movie.getId());
                    fragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, fragment, "details");
                    fragmentTransaction.addToBackStack("details");
                    fragmentTransaction.commit();
                }
            });
        }
    }
}
