package com.example.moviebrowser;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.moviebrowser.model.Movie;

public class MovieDetailFragment extends Fragment {
    private Movie movie = null;

    public MovieDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            int movieId = getArguments().getInt("movieId");
            movie = ((MainActivity)getActivity()).sqliteDB.getMovieById(movieId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);

        if (movie != null) {
            ((TextView) view.findViewById(R.id.movieTitle)).setText(movie.getTitle());
            String sub = "(" + movie.getGenre().getGenre() + " - IMDB: " + movie.getImdb_rate() + " / 10 )";
            ((TextView) view.findViewById(R.id.movieGenre)).setText(sub);
            ((TextView) view.findViewById(R.id.movieDirector)).setText(movie.getDirector().toString());
            ((TextView) view.findViewById(R.id.movieCast)).setText(movie.getActorNames());
            ((TextView) view.findViewById(R.id.movieDescription)).setText(movie.getDescription());
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity)getActivity()).getSupportActionBar().show();
    }
}
