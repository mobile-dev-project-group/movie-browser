package com.example.moviebrowser;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moviebrowser.model.Actor;
import com.example.moviebrowser.model.Director;
import com.example.moviebrowser.model.Genre;
import com.example.moviebrowser.model.Studio;

import java.util.ArrayList;
import java.util.List;


public class NewItemFragment extends Fragment {
    String title;
    Studio selectedStudio;
    Director selectedDirector;
    Actor selectedActor;
    Actor selectedMovieActor;
    List<Actor> movieActors = new ArrayList<>();
    Genre selectedGenre;
    String release;
    String budget;
    String description;
    String rating;
    Spinner newStudioSpinner;
    Spinner newDirectorSpinner;
    Spinner newGenreSpinner;
    Spinner newActorSpinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_new_item, container, false);

        //////////////////////////////
        // Studio START
        newStudioSpinner = rootView.findViewById(R.id.newStudioSpinner);

        rootView.findViewById(R.id.newStudioButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Add Studio");
                View dialogview = LayoutInflater.from(getContext()).inflate(R.layout.dialog_new_studio, (ViewGroup) getView(), false);
                final EditText newDialogStudioEditText = dialogview.findViewById(R.id.newDialogStudioEditText);
                builder.setView(dialogview);

                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newStudioName = newDialogStudioEditText.getText().toString();
                        if (newStudioName.trim().length() > 0) {
                            Studio studio = new Studio();
                            studio.setName(newStudioName);
                            MainActivity.sqliteDB.addNewStudio(studio);

                            // loading spinner with newly added data
                            loadNewStudioSpinnerData();
                            dialog.dismiss();
                        }
                        else {
                            Toast.makeText(getContext(), "Please enter studio name", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
        loadNewStudioSpinnerData();

        newStudioSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedStudio = (Studio)parent.getSelectedItem();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        // Studio END
        //////////////////////////////

        //////////////////////////////
        // Director START
        newDirectorSpinner = rootView.findViewById(R.id.newDirectorSpinner);

        rootView.findViewById(R.id.newDirectorButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Add Director");
                View dialogview = LayoutInflater.from(getContext()).inflate(R.layout.dialog_new_director, (ViewGroup) getView(), false);
                final EditText newDialogDirectorLastname = dialogview.findViewById(R.id.newDialogDirectorLastnameEditText);
                final EditText newDialogDirectorFirstname = dialogview.findViewById(R.id.newDialogDirectorFirstnameEditText);
                final EditText newDialogDirectorCountry = dialogview.findViewById(R.id.newDialogDirectorCountryEditText);
                builder.setView(dialogview);

                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newDirectorLastName = newDialogDirectorLastname.getText().toString();
                        String newDirectorFirstName = newDialogDirectorFirstname.getText().toString();
                        String newDirectorCountry = newDialogDirectorCountry.getText().toString();
                        if ((newDirectorLastName.trim().length() > 0) && (newDirectorFirstName.trim().length() > 0) && (newDirectorCountry.trim().length() > 0)) {
                            Director director = new Director();
                            director.setLast_name(newDirectorLastName);
                            director.setFirst_name(newDirectorFirstName);
                            director.setCountry(newDirectorCountry);
                            MainActivity.sqliteDB.addNewDirector(director);

                            // loading spinner with newly added data
                            loadNewDirectorSpinnerData();
                            dialog.dismiss();
                        } else {
                            if (newDirectorLastName.trim().length() == 0) {
                                Toast.makeText(getContext(), "Please enter director last name", Toast.LENGTH_SHORT).show();
                            } else if (newDirectorFirstName.trim().length() == 0) {
                                Toast.makeText(getContext(), "Please enter director first name", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Please enter director country", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
        loadNewDirectorSpinnerData();

        newDirectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDirector = (Director)parent.getSelectedItem();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        // Director END
        //////////////////////////////

        //////////////////////////////
        // Actor START
        newActorSpinner = rootView.findViewById(R.id.newActorSpinner);

        rootView.findViewById(R.id.newActorButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Add Actor");
                View dialogview = LayoutInflater.from(getContext()).inflate(R.layout.dialog_new_actor, (ViewGroup) getView(), false);
                final EditText newDialogActorLastname = dialogview.findViewById(R.id.newDialogActorLastnameEditText);
                final EditText newDialogActorFirstname = dialogview.findViewById(R.id.newDialogActorFirstnameEditText);
                final EditText newDialogActorCountry = dialogview.findViewById(R.id.newDialogActorCountryEditText);
                builder.setView(dialogview);

                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newActorLastName = newDialogActorLastname.getText().toString();
                        String newActorFirstName = newDialogActorFirstname.getText().toString();
                        String newActorCountry = newDialogActorCountry.getText().toString();
                        if ((newActorLastName.trim().length() > 0) && (newActorFirstName.trim().length() > 0) && (newActorCountry.trim().length() > 0)) {
                            Actor actor = new Actor();
                            actor.setLast_name(newActorLastName);
                            actor.setFirst_name(newActorFirstName);
                            actor.setCountry(newActorCountry);
                            MainActivity.sqliteDB.addNewActor(actor);

                            // loading spinner with newly added data
                            loadNewActorSpinnerData();
                            dialog.dismiss();
                        } else {
                            if (newActorLastName.trim().length() == 0) {
                                Toast.makeText(getContext(), "Please enter actor last name", Toast.LENGTH_SHORT).show();
                            } else if (newActorFirstName.trim().length() == 0) {
                                Toast.makeText(getContext(), "Please enter actor first name", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Please enter actor country", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
        loadNewActorSpinnerData();

        newActorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedActor = (Actor)parent.getSelectedItem();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Actor Add/del in ListView
        final ArrayAdapter<Actor> actors = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, movieActors);
        ((ListView)rootView.findViewById(R.id.newActorListView)).setAdapter(actors);
        rootView.findViewById(R.id.newActorAddListButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedActor != null && !movieActors.contains(selectedActor)) {
                    actors.add(selectedActor);
                    actors.notifyDataSetChanged();
                }
            }
        });
        rootView.findViewById(R.id.newActorDelListButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedMovieActor != null) {
                    if (movieActors.contains(selectedMovieActor)) {
                        actors.remove(selectedMovieActor);
                        actors.notifyDataSetChanged();
                    }
                }
                else {
                    Toast.makeText(getContext(), "No selection!", Toast.LENGTH_LONG).show();
                }
            }
        });
        ((ListView)rootView.findViewById(R.id.newActorListView)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                actors.remove((Actor)parent.getItemAtPosition(position));
                actors.notifyDataSetChanged();
            }
        });
        // Actor END
        //////////////////////////////

        //////////////////////////////
        // Genre START
        newGenreSpinner = rootView.findViewById(R.id.newGenreSpinner);

        rootView.findViewById(R.id.newGenreButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Add Genre");
                View dialogview = LayoutInflater.from(getContext()).inflate(R.layout.dialog_new_genre, (ViewGroup) getView(), false);
                final EditText newDialogGenreEditText = dialogview.findViewById(R.id.newDialogGenreEditText);
                builder.setView(dialogview);

                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newGenreNameEditText = newDialogGenreEditText.getText().toString();
                        if (newGenreNameEditText.trim().length() > 0) {
                            Genre genre = new Genre();
                            genre.setGenre(newGenreNameEditText);
                            MainActivity.sqliteDB.addNewGenre(genre);

                            // loading spinner with newly added data
                            loadNewGenreSpinnerData();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getContext(), "Please enter Genre",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
        loadNewGenreSpinnerData();

        newGenreSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGenre = (Genre)parent.getSelectedItem();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        // Genre END
        //////////////////////////////

        //////////////////////////////
        // SeekBar START
        SeekBar seekBar = rootView.findViewById(R.id.newSeekBar);
        final TextView textView = rootView.findViewById(R.id.newSeekBarTextView);
        seekBar.setMax(100);
        seekBar.setProgress(50);
        rating = getConvertedValue(seekBar.getProgress());
        textView.setText(rating + " / " + seekBar.getMax() / 10);
        seekBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        int min = 10;
                        int temp = progress;

                        if(progress < min) {
                            seekBar.setProgress(min);
                            temp = min;
                        }

                        rating = getConvertedValue(temp);
                        textView.setText(rating + " / " + seekBar.getMax() / 10);
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        // SeekBar END
        //////////////////////////////

        //////////////////////////////
        // SaveButton START
        rootView.findViewById(R.id.newMovieUploadButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title = ((EditText)getView().findViewById(R.id.newTitleOfMovieEditText)).getText().toString();
                release = ((EditText)getView().findViewById(R.id.newReleaseEditText)).getText().toString();
                description = ((EditText)getView().findViewById(R.id.newDescriptionEditText)).getText().toString();
                budget = ((EditText)getView().findViewById(R.id.newBudgetEditText)).getText().toString();

                if (title == null || title.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please enter the Title", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (selectedStudio == null) {
                    Toast.makeText(getContext(), "Please add a Studio", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (selectedDirector == null) {
                    Toast.makeText(getContext(), "Please add a Director", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (selectedGenre == null) {
                    Toast.makeText(getContext(), "Please add a Genre", Toast.LENGTH_SHORT).show();
                    return;
                }
                
                if (movieActors.size() == 0) {
                    Toast.makeText(getContext(), "Please add Actors to the movie", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (release == null || release.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please enter the Release", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (description == null || description.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please enter the description", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (budget == null || budget.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please enter the Budget", Toast.LENGTH_SHORT).show();
                    return;
                }
                
                if (rating == null || rating.trim().isEmpty()) {
                    Toast.makeText(getContext(), "Please enter the rating", Toast.LENGTH_SHORT).show();
                    return;
                }
                
                long movieId = ((MainActivity)getActivity()).sqliteDB.addMovie(
                        title,
                        selectedStudio,
                        selectedDirector,
                        selectedGenre,
                        release,
                        description,
                        budget,
                        rating
                );

                for (Actor actor : movieActors) {
                    ((MainActivity)getActivity()).sqliteDB.addActorToMovie(movieId, actor);
                }

                getFragmentManager().popBackStackImmediate();
            }
        });
        // SaveButton END
        //////////////////////////////

        return rootView;
    }


    //////////////////////////////
    // Studio spinner START
    private void loadNewStudioSpinnerData() {
        List<Studio> studios = MainActivity.sqliteDB.getAllStudio();
        ArrayAdapter<Studio> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, studios);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        newStudioSpinner.setAdapter(arrayAdapter);
    }
    // Studio Spinner END
    //////////////////////////////

    //////////////////////////////
    // Director spinner START
    private void loadNewDirectorSpinnerData() {
        List<Director> directors = MainActivity.sqliteDB.getAllDirector();
        ArrayAdapter<Director> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, directors);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        newDirectorSpinner.setAdapter(arrayAdapter);
    }
    // Director Spinner END
    //////////////////////////////

    //////////////////////////////
    // Actor spinner START
    private void loadNewActorSpinnerData() {
        List<Actor> actors = MainActivity.sqliteDB.getAllActor();
        ArrayAdapter<Actor> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, actors);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        newActorSpinner.setAdapter(arrayAdapter);
    }
    // Actor Spinner END
    //////////////////////////////

    //////////////////////////////
    // Genre spinner START
    private void loadNewGenreSpinnerData() {
        List<Genre> genres = MainActivity.sqliteDB.getAllGenre();
        ArrayAdapter<Genre> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, genres);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        newGenreSpinner.setAdapter(arrayAdapter);
    }
    // Genre Spinner END
    //////////////////////////////

    // SeekBar Converter
    public String getConvertedValue(int intVal) {
        @SuppressLint("DefaultLocale") String DecimalFormat = String.format("%.1f",0.1f * intVal);
        return DecimalFormat;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity)getActivity()).getSupportActionBar().show();
    }
}
