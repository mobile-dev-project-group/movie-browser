package com.example.moviebrowser;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.moviebrowser.model.Actor;
import com.example.moviebrowser.model.Director;
import com.example.moviebrowser.model.Genre;
import com.example.moviebrowser.model.Movie;
import com.example.moviebrowser.model.Studio;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME ="movie_database.db";
    SQLiteDatabase db;
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS \"actor\" (\n" +
                "\t\"id\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"first_name\"\tTEXT NOT NULL,\n" +
                "\t\"last_name\"\tTEXT NOT NULL,\n" +
                "\t\"country\"\tTEXT\n" +
                ");");
        db.execSQL("CREATE TABLE IF NOT EXISTS \"director\" (\n" +
                "\t\"id\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"first_name\"\tTEXT NOT NULL,\n" +
                "\t\"last_name\"\tTEXT NOT NULL,\n" +
                "\t\"country\"\tINTEGER\n" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS \"genre\" (\n" +
                "\t\"id\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"genre\"\tTEXT NOT NULL UNIQUE\n" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS \"studio\" (\n" +
                "\t\"id\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"name\"\tTEXT NOT NULL UNIQUE\n" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS \"movie\" (\n" +
                "\t\"id\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"title\"\tTEXT NOT NULL UNIQUE,\n" +
                "\t\"studio_id\"\tINTEGER NOT NULL,\n" +
                "\t\"director_id\"\tINTEGER NOT NULL,\n" +
                "\t\"genre_id\"\tINTEGER NOT NULL,\n" +
                "\t\"release\"\tTEXT NOT NULL,\n" +
                "\t\"description\"\tTEXT NOT NULL,\n" +
                "\t\"budget\"\tTEXT,\n" +
                "\t\"imdb_rate\"\tTEXT,\n" +
                "\tFOREIGN KEY(\"studio_id\") REFERENCES \"studio\"(\"id\"),\n" +
                "\tFOREIGN KEY(\"director_id\") REFERENCES \"director\"(\"id\"),\n" +
                "\tFOREIGN KEY(\"genre_id\") REFERENCES \"genre\"(\"id\")\n" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS \"movie_actors\" (\n" +
                "\t\"actor_id\"\tINTEGER NOT NULL,\n" +
                "\t\"movie_id\"\tINTEGER NOT NULL,\n" +
                "\tFOREIGN KEY(\"actor_id\") REFERENCES \"actor\"(\"id\"),\n" +
                "\tFOREIGN KEY(\"movie_id\") REFERENCES \"movie\"(\"id\")\n" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS movie_actors");
        db.execSQL("DROP TABLE IF EXISTS movie");
        db.execSQL("DROP TABLE IF EXISTS actor");
        db.execSQL("DROP TABLE IF EXISTS director");
        db.execSQL("DROP TABLE IF EXISTS studio");
        db.execSQL("DROP TABLE IF EXISTS genre");
    }

    public void addNewDirector(Director director){
        db.execSQL("INSERT INTO director(last_name, first_name, country) " +
                "values(\""+ director.getLast_name() +"\", \"" + director.getFirst_name() + "\", \""+ director.getCountry() +"\")");
    }
    public void addNewActor(Actor actor){
        db.execSQL("INSERT INTO actor(last_name, first_name, country) " +
                "values(\""+ actor.getLast_name() +"\", \"" + actor.getFirst_name() + "\", \""+ actor.getCountry() +"\")");
    }
    public void addNewStudio(Studio studio){
        db.execSQL("INSERT INTO studio(name) " +
                "values(\"" + studio.getName() + "\")");
    }
    public void addNewGenre(Genre genre){
        db.execSQL("INSERT INTO genre(genre) " +
                "values(\"" + genre.getGenre() + "\")");
    }
    public void addNewMovie(String TITLE, Studio studio, Director director, Genre genre,
                            String RELEASE, String DESCRIPTION, String BUDGET, String IMBD_RATE ) {
        db.execSQL("INSERT INTO movie(title, studio_id, director_id, genre_id, release, description, budget, imbdb_rate) " +
                "values(\"" + TITLE + "\", "+ studio.getId() +", "+ director.getId() + ", " +
                genre.getId() +" ,\"" + RELEASE + "\", \"" + DESCRIPTION + "\", " + BUDGET +
                ", "+ IMBD_RATE +")");
    }
    public long addMovie(String TITLE, Studio studio, Director director, Genre genre, String RELEASE, String DESCRIPTION, String BUDGET, String IMBD_RATE) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", TITLE);
        contentValues.put("studio_id", studio.getId());
        contentValues.put("director_id", director.getId());
        contentValues.put("genre_id", genre.getId());
        contentValues.put("release", RELEASE);
        contentValues.put("description", DESCRIPTION);
        contentValues.put("budget", BUDGET);
        contentValues.put("imdb_rate", IMBD_RATE);
        return db.insert("movie", null, contentValues);
    }
    public void addActorToMovie(Movie movie, Actor actor){
        db.execSQL("INSERT INTO movie_actors(actor_id, movie_id) VALUES("+ actor.getId() +", "+ movie.getId()+")");
    }
    public void addActorToMovie(long movieId, Actor actor){
        db.execSQL("INSERT INTO movie_actors(actor_id, movie_id) VALUES("+ actor.getId() +", "+ movieId+")");
    }

    public Actor getActorById(int id){
        Actor act = new Actor();
        Cursor c = db.rawQuery("SELECT * FROM actor WHERE id=" + id, null);
        c.moveToFirst();
        act.setId(c.getInt(0));
        act.setFirst_name(c.getString(1));
        act.setLast_name(c.getString(2));
        act.setCountry(c.getString(3));
        // closing connection
        c.close();
        //db.close();
        // returning lables
        return act;
    }
    public Director getDirectorBy(int id){
        Director dir = new Director();
        Cursor c = db.rawQuery("SELECT * FROM director WHERE id=" + id, null);
        c.moveToFirst();
        dir.setId(c.getInt(0));
        dir.setFirst_name(c.getString(1));
        dir.setLast_name(c.getString(2));
        dir.setCountry(c.getString(3));
        // closing connection
        c.close();
        //db.close();
        // returning lables
        return dir;
    }
    public Studio getStudioById(int id){
        Studio studio = new Studio();
        Cursor c = db.rawQuery("SELECT * FROM studio WHERE id=" + id, null);
        c.moveToFirst();
        studio.setId(c.getInt(0));
        studio.setName(c.getString(1));
        // closing connection
        c.close();
        //db.close();
        // returning lables
        return studio;
    }
    public Genre getGenreById(int id){
        Genre genre = new Genre();
        Cursor c = db.rawQuery("SELECT * FROM genre WHERE id=" + id, null);
        c.moveToFirst();
        genre.setId(c.getInt(0));
        genre.setGenre(c.getString(1));
        // closing connection
        c.close();
        //db.close();
        // returning lables
        return genre;
    }
    public Movie getMovieById(int id){
        Movie movie = new Movie();
        Cursor c = db.rawQuery("SELECT * FROM movie WHERE id=" + id, null);
        c.moveToFirst();
        movie.setId(c.getInt(0));
        movie.setTitle(c.getString(1));
        movie.setStudio(getStudioById(c.getInt(2)));
        movie.setDirector(getDirectorBy(c.getInt(3)));
        movie.setGenre(getGenreById(c.getInt(4)));
        movie.setRelease(c.getString(5));
        movie.setDescription(c.getString(6));
        movie.setBudget(c.getString(7));
        movie.setImdb_rate(c.getString(8));
        movie.setActors(getActorsToMovieById(c.getInt(0)));
        // closing connection
        c.close();
        //db.close();
        // returning lables
        return movie;
    }
    public List<Actor> getActorsToMovieById(int id){
        List<Actor> list_actor = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM movie_actors WHERE movie_id=" + id, null);
        while (c.moveToNext()) {
            list_actor.add(getActorById(c.getInt(0)));
        }
        // closing connection
        c.close();
        //db.close();
        // returning lables
        return list_actor;
    }
    public List<Movie> getAllMovie(){
        List<Movie> movie_list = new  ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM movie", null);
        while (c.moveToNext()) {
            movie_list.add(getMovieById(c.getInt(0)));
        }
        // closing connection
        c.close();
        //db.close();
        // returning lables
        return movie_list;
    }

    public List<Studio> getAllStudio(){
        ArrayList<Studio> studio_list = new  ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM studio", null);
        while (c.moveToNext()) {
            studio_list.add(getStudioById(c.getInt(0)));
        }
        // closing connection
        c.close();
        // returning tables
        return studio_list;
    }

    public List<Genre> getAllGenre(){
        ArrayList<Genre> genre_list = new  ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM genre", null);
        while (c.moveToNext()) {
            genre_list.add(getGenreById(c.getInt(0)));
        }
        // closing connection
        c.close();
        // returning tables
        return genre_list;
    }

    public List<Director> getAllDirector(){
        List<Director> director_list = new  ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM director", null);
        while (c.moveToNext()) {
            director_list.add(getDirectorBy(c.getInt(0)));
        }
        // closing connection
        c.close();
        // db.close();
        // returning lables
        return director_list;
    }

    public List<Actor> getAllActor(){
        List<Actor> actor_list = new  ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM actor", null);
        while (c.moveToNext()) {
            actor_list.add(getActorById(c.getInt(0)));
        }
        // closing connection
        c.close();
        // db.close();
        // returning lables
        return actor_list;
    }

    public void dropDatabase(Context context)
    {
        db.close();
        context.deleteDatabase(DATABASE_NAME);
    }
}
