package com.example.moviebrowser.model;

import android.support.annotation.NonNull;

public class Genre {
    private int id;
    private String genre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @NonNull
    @Override
    public String toString() {
        return this.genre;
    }
}
